
# SIG名称

消息中间件SIG

# 该SIG的业务范围
 1） 维护上游开源社区的消息中间件软件包（比如，Apache RocketMQ）；
 2） 在欧拉社区孵化与开源自研的消息中间件（比如、移动云、华为云、京东云和腾讯云内部自研的消息中间件）；
 3） 定期组织关于消息中间件的线上会议与Meetup技术研讨会议；
 4） 给业界提供基于欧拉系统和基础软件的消息中间件解决方案；
 5） 探索基于欧拉的消息中间件加速方案；


# sig组例会安排及议题申报方法
双周会（周三晚上，19：30）
固定议题：
1）重点项目进度汇报
2）新项目评审
3）例行审视meetup演讲主题（月度）
议题申报：邮件形式
例会组织：轮值主持和记要（胡宗棠组织安排，不限于核心成员）

# Meetup（项目、技术分享会）
时间不固定，提前群里发在线会议链接，有兴趣的公司成员或学生均可参与


# 成员
### Maintainer列表
- 胡宗棠[@hu_zongtang] 邮箱：huzongtang_yewu@cmss.chinamobile.com 
- 江海挺[@jas0n918] 邮箱：jason.918@qq.com

### Committer列表
- 章及第[@zhangjidi] 邮箱：zhangjidi_yewu@cmss.chinamobile.com
- 吴碧清[@wubiqing] 邮箱：wubiqing_yewu@cmss.chinamobile.com
- 王嘉凌[@wang-jialing218] 邮箱：wangjialing_yewu@cmss.chinamobile.com

# 联系方式
- [邮件列表](message-middleware@openeuler.org)


# 项目repository地址:

- https://gitee.com/src-openeuler/rocketmq
- https://gitee.com/src-openeuler/rabbitmq-java-client
- https://gitee.com/src-openeuler/rabbitmq-server 
- https://gitee.com/src-openeuler/pulsar
